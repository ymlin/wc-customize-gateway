<?php

/*
Plugin Name: my Shipping plugin
Plugin URI: http://woothemes.com/woocommerce
Description:my shipping method plugin
Version: 1.0.0
Author: WooThemes
Author URI: http://woothemes.com
*/

 

$active_plugins2 = apply_filters('active_plugins', get_option('active_plugins'));

if (in_array('woocommerce/woocommerce.php', $active_plugins2)) {
    require_once __DIR__ . '/wc-my-home.php';
	 require_once __DIR__ . '/wc-your-home.php';
	  require_once __DIR__ . '/wc-they-home.php';
    if (!function_exists('get_woo_version')) {
        function get_woo_version2()
        {
            return WC()->version;
        }
    }

}
